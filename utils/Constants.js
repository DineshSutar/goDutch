export default {
  PROFESSION: ['Student', 'Professional'],
  VALIDATION_REGEX: {
    MOBILE_NO: /^\d{1,10}$/,
    UPI_ID: /^([\w.-]+[@][a-zA-Z]+)$/,
    FULL_NAME: /^([a-zA-Z'\- ])+$/,
  },
};
