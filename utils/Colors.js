export default {
  transparent: '#00000000',
  white: '#FFFFFF',
  black: '#000000',
  red: '#E60000',
  blue: '#5A67F2',
  screenContainerBg: '#F8F8F8',
  borderColor: '#F1F1F1',
  darkText: "#333333",
  lightText: '#444444',
};
