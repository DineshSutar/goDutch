import React, {PureComponent} from 'react';
import {Image, View} from 'react-native';

import {Button, Card, Divider, ScreenContainer, TextInput} from '../components';
import {Routes, Images, CONSTANTS} from '../utils';
import {HomeScreenStyles} from './Styles';

export default class Home extends PureComponent {
  state = {
    mobileNo: undefined,
    error: undefined,
  };

  _renderBrandName() {
    const {brandingContainer, brandingLogo} = HomeScreenStyles;
    const {icon, brandText} = Images;
    return (
      <View style={brandingContainer}>
        <Image source={icon} style={brandingLogo} />
        <Image source={brandText} />
      </View>
    );
  }

  _onPressContinue = () => {
    const {mobileNo, error} = this.state;
    if (!error) {
      if (mobileNo && mobileNo.length === 10) {
        this.props.navigation.navigate(Routes.ACCOUNT_SETUP, {mobileNo});
      } else {
        this.setState({error: 'Please enter a valid 10 digit mobile no.'});
      }
    }
  };

  _onChangeMobileNo = mobileNo => {
    const error = CONSTANTS.VALIDATION_REGEX.MOBILE_NO.test(mobileNo)
      ? undefined
      : 'Please enter numbers only';
    this.setState({
      mobileNo,
      error,
    });
  };

  render() {
    const {mobileNo, error} = this.state;
    const {
      screenContainer,
      mobileNoTextInput,
      continueButton,
    } = HomeScreenStyles;
    return (
      <ScreenContainer style={screenContainer}>
        <Card>
          {this._renderBrandName()}
          <Divider />
          <TextInput
            autoFocus
            label="Mobile Number"
            isRequired
            placeholder="xxxxxxxxxx"
            containerStyle={mobileNoTextInput}
            returnKeyType="next"
            keyboardType="phone-pad"
            maxLength={10}
            onChangeText={this._onChangeMobileNo}
            value={mobileNo}
            error={error}
            onSubmitEditing={this._onPressContinue}
          />
          <Button
            title="Continue"
            style={continueButton}
            onPress={this._onPressContinue}
          />
        </Card>
      </ScreenContainer>
    );
  }
}
