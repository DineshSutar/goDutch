import {StyleSheet} from 'react-native';

import {Colors} from '../utils';

export const HomeScreenStyles = StyleSheet.create({
  brandingContainer: {
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    paddingVertical: 30,
  },
  brandingLogo: {marginHorizontal: 5, height: 50, width: 50},
  screenContainer: {flex: 1, justifyContent: 'center'},
  mobileNoTextInput: {paddingVertical: 40},
  continueButton: {marginBottom: 40, alignSelf: 'center'},
});

export const AccountSetupScreenStyles = StyleSheet.create({
  logoContainer: {
    alignSelf: 'center',
    height: 100,
    width: 100,
    borderRadius: 100,
    backgroundColor: Colors.screenContainerBg,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20,
  },
  logo: {height: 80, width: 80},
  professionSelectorContainer: {width: '100%', paddingVertical: 10},
  professionSelectorLabel: {
    fontFamily: 'Montserrat-Medium',
    color: Colors.darkText,
    padding: 5,
    fontSize: 15,
  },
  professionButtonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 10,
  },
  professionButtonTitle: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 15,
  },
  professionButton: {
    width: '48%',
    backgroundColor: Colors.white,
    borderWidth: 1,
  },
  continueButton: {marginVertical: 30, alignSelf: 'center'},
});

export const AccountDetailsScreenStyles = StyleSheet.create({
  card: {paddingBottom: 30},
  continueButton: {alignSelf: 'center', marginTop: 40},
});
