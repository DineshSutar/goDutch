import React, {PureComponent, createRef} from 'react';
import {View, Image} from 'react-native';

import {Images, CONSTANTS, Colors, Routes} from '../utils';
import {Button, TextInput, Card, Text, ScreenContainer} from '../components';
import {AccountSetupScreenStyles} from './Styles';

export default class AccountSetup extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      fullName: undefined,
      upiID: undefined,
      profession: CONSTANTS.PROFESSION[0],
      refresh: false,
    };
    this.upiIDInput = createRef();
  }

  _onChangeFullName = fullName => {
    if (this.fullNameError) {
      this.fullNameError = undefined;
    }
    this.setState({fullName});
  };

  _onChangeUpiID = upiID => {
    if (this.upiIDError) {
      this.upiIDError = undefined;
    }
    this.setState({upiID});
  };

  _validate(value, regex) {
    return !!value && value.trim().length !== 0 && regex.test(value);
  }

  _onPressContinue = () => {
    const {fullName, upiID, profession} = this.state;
    const {FULL_NAME, UPI_ID} = CONSTANTS.VALIDATION_REGEX;
    this.fullNameError = this._validate(fullName, FULL_NAME)
      ? undefined
      : 'Please enter a valid full name';
    this.upiIDError = this._validate(upiID, UPI_ID)
      ? undefined
      : 'Please enter a valid UPI ID';
    if (this.fullNameError || this.upiIDError) {
      this._refresh();
    } else {
      const {
        navigation: {navigate},
        route: {
          params: {mobileNo},
        },
      } = this.props;
      navigate(Routes.ACCOUNT_DETAILS, {
        mobileNo,
        fullName,
        upiID,
        profession,
      });
    }
  };

  _onSubmitFullName = () => {
    if (
      !this._validate(this.state.fullName, CONSTANTS.VALIDATION_REGEX.FULL_NAME)
    ) {
      this.fullNameError = 'Please enter a valid full name';
      this._refresh();
    }
    this.upiIDInput && this.upiIDInput.current.focus();
  };

  _onPressProfessionSelector(profession) {
    if (this.state.profession !== profession) {
      this.setState({profession});
    }
  }

  _refresh() {
    this.setState(prevState => ({refresh: !prevState.refresh}));
  }

  _renderProfessionSelector() {
    const {
      professionSelectorContainer,
      professionSelectorLabel,
      professionButtonsContainer,
      professionButtonTitle,
      professionButton,
    } = AccountSetupScreenStyles;
    return (
      <View style={professionSelectorContainer}>
        <Text style={professionSelectorLabel}>Current Profession</Text>
        <View style={professionButtonsContainer}>
          {CONSTANTS.PROFESSION.map(p => {
            const isSelected = this.state.profession === p;
            return (
              <Button
                key={p}
                title={p}
                onPress={() => this._onPressProfessionSelector(p)}
                titleStyle={[
                  professionButtonTitle,
                  {color: isSelected ? Colors.blue : Colors.lightText},
                ]}
                style={[
                  professionButton,
                  {
                    borderColor: isSelected
                      ? Colors.blue
                      : Colors.borderColor,
                  },
                ]}
              />
            );
          })}
        </View>
      </View>
    );
  }

  _renderLogo() {
    const {logoContainer, logo} = AccountSetupScreenStyles;
    return (
      <View style={logoContainer}>
        <Image source={Images.icon} style={logo} />
      </View>
    );
  }

  render() {
    const {fullName, upiID} = this.state;
    const {continueButton} = AccountSetupScreenStyles;
    return (
      <ScreenContainer headerTitle="Setup your GoDutch account">
        <Card>
          {this._renderLogo()}
          {this._renderProfessionSelector()}
          <TextInput
            autoFocus
            label="Full Name"
            isRequired
            placeholder="eg: Firstname Lastname"
            returnKeyType="next"
            onChangeText={this._onChangeFullName}
            value={fullName}
            onSubmitEditing={this._onSubmitFullName}
            error={this.fullNameError}
          />
          <TextInput
            ref={this.upiIDInput}
            label="UPI ID"
            isRequired
            placeholder="eg: abc@oksbi"
            returnKeyType="next"
            onChangeText={this._onChangeUpiID}
            value={upiID}
            onSubmitEditing={this._onPressContinue}
            error={this.upiIDError}
          />
          <Button
            title="Continue"
            style={continueButton}
            onPress={this._onPressContinue}
          />
        </Card>
      </ScreenContainer>
    );
  }
}
