import React, {PureComponent} from 'react';

import {Button, DetailItem, Card, ScreenContainer} from '../components';
import {Routes} from '../utils';
import {AccountDetailsScreenStyles} from './Styles';

export default class AccountDetails extends PureComponent {
  _onPressContinue = () =>
    this.props.navigation.reset({
      routes: [{name: Routes.HOME}],
    });

  render() {
    const {fullName, mobileNo, profession, upiID} = this.props.route.params;
    const {card, continueButton} = AccountDetailsScreenStyles;
    return (
      <ScreenContainer headerTitle="Your Details">
        <Card style={card}>
          <DetailItem name="Name" value={fullName} />
          <DetailItem name="Mobile number" value={mobileNo} />
          <DetailItem name="UPI ID" value={upiID} />
          <DetailItem name="Profession" value={profession} />
        </Card>
        <Button
          title="Continue"
          onPress={this._onPressContinue}
          style={continueButton}
        />
      </ScreenContainer>
    );
  }
}
