import React from 'react';
import {
  SafeAreaView,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from 'react-native';

import Text from './Text';
import {ScreenContainerStyles} from './Styles';

export default ({children, style, headerTitle}) => {
  const {safeArea, container, headerText} = ScreenContainerStyles;
  return (
    <SafeAreaView style={safeArea}>
      <KeyboardAvoidingView
        style={safeArea}
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
        {!!headerTitle && <Text style={headerText}>{headerTitle}</Text>}
        <ScrollView
          contentContainerStyle={[container, style]}
          keyboardShouldPersistTaps="handled"
          bounces="false">
          {children}
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};
