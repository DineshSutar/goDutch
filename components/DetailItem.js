import React from 'react';

import Text from './Text';
import Divider from './Divider';
import {DetailItemStyles} from './Styles';

export default ({name, value}) => {
  const {detailText, detailValueText} = DetailItemStyles;
  return (
    <>
      <Text style={detailText}>{name}</Text>
      <Divider />
      <Text style={[detailText, detailValueText]}>{value || '--'}</Text>
      <Divider />
    </>
  );
};
