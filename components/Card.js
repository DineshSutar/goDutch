import React from 'react';
import {View} from 'react-native';

import {CardStyles} from './Styles';

export default ({children, style}) => (
  <View style={[CardStyles.card, style]}>{children}</View>
);
