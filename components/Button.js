import React from 'react';
import {TouchableOpacity} from 'react-native';

import Text from './Text';
import {ButtonStyles} from './Styles';

export default ({title, titleStyle, style, disabled, onPress}) => {
  const { button, text } = ButtonStyles; 
  return (
    <TouchableOpacity
      style={[button, style]}
      disabled={disabled}
      onPress={onPress}>
      <Text style={[text, titleStyle]}>{title}</Text>
    </TouchableOpacity>
  );
};
