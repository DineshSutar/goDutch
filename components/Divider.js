import React from 'react';
import {View} from 'react-native';

import {DividerStyles} from './Styles';

export default props => <View style={DividerStyles.divider} {...props} />;
