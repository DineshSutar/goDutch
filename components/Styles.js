import {StyleSheet} from 'react-native';

import {Colors} from '../utils';

export const TextInputStyles = StyleSheet.create({
  inputContainer: {paddingVertical: 10},
  labelStyle: {
    fontFamily: 'Montserrat-Medium',
    color: Colors.darkText,
    padding: 5,
    fontSize: 15,
  },
  required: {color: Colors.red},
  errorText: {
    color: Colors.red,
    paddingHorizontal: 5,
    paddingTop: 3,
  },
  input: {
    fontFamily: 'Montserrat-Regular',
    padding: 8,
    color: Colors.lightText,
    fontSize: 15,
    backgroundColor: Colors.transparent,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: Colors.borderColor,
    width: '100%',
    height: 50,
  },
});

export const CardStyles = StyleSheet.create({
  card: {
    width: '100%',
    backgroundColor: Colors.white,
    borderRadius: 5,
    padding: 15,
    elevation: 5,
    shadowColor: Colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
    shadowRadius: 3,
  },
});

export const ButtonStyles = StyleSheet.create({
  button: {
    borderRadius: 5,
    backgroundColor: Colors.blue,
    width: 170,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontFamily: 'Montserrat-Medium',
    color: Colors.white,
  },
});

export const DetailItemStyles = StyleSheet.create({
  detailText: {
    fontSize: 15,
    fontFamily: 'Montserrat-Medium',
    paddingVertical: 15,
    paddingHorizontal: 5,
  },
  detailValueText: {color: Colors.blue},
});

export const DividerStyles = StyleSheet.create({
  divider: {
    height: 1,
    width: '100%',
    borderColor: Colors.borderColor,
    borderWidth: 1,
    borderStyle: 'dashed',
  },
});

export const ScreenContainerStyles = StyleSheet.create({
  safeArea: {flex: 1, backgroundColor: Colors.screenContainerBg},
  container: {padding: 10},
  headerText: {paddingHorizontal: 10, paddingTop: 10},
});

export const TextStyles = StyleSheet.create({
  defaultText: {
    fontFamily: 'Montserrat-Regular',
    color: Colors.lightText,
    fontSize: 14,
  },
});
