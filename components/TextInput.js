import React, {forwardRef} from 'react';
import {TextInput as RNTextInput, View} from 'react-native';

import Text from './Text';
import {Colors} from '../utils';
import {TextInputStyles} from './Styles';

export default forwardRef(
  (
    {
      label,
      isRequired,
      placeholder,
      containerStyle,
      autoFocus,
      returnKeyType,
      value,
      keyboardType,
      maxLength,
      onChangeText,
      error,
      onEndEditing,
      onSubmitEditing,
    },
    ref,
  ) => {
    const {
      inputContainer,
      labelStyle,
      errorText,
      input,
      required,
    } = TextInputStyles;
    return (
      <View style={[inputContainer, containerStyle]}>
        <Text style={labelStyle}>
          {label}
          {!!isRequired && <Text style={required}>*</Text>}
        </Text>
        <RNTextInput
          ref={ref}
          style={input}
          placeholder={placeholder}
          placeholderTextColor={Colors.borderColor}
          autoFocus={autoFocus}
          returnKeyType={returnKeyType}
          value={value}
          keyboardType={keyboardType}
          maxLength={maxLength}
          onChangeText={onChangeText}
          onEndEditing={onEndEditing}
          onSubmitEditing={onSubmitEditing}
        />
        <Text style={errorText}>{error || ' '}</Text>
      </View>
    );
  },
);
