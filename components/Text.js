import React from 'react';
import {Text as RNText, StyleSheet} from 'react-native';
import Colors from '../utils/Colors';

import {TextStyles} from './Styles';

export default ({children, style}) => (
  <RNText style={[TextStyles.defaultText, style]}>{children}</RNText>
);
