import React from 'react';
import {StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import {Home, AccountSetup, AccountDetails} from './screens';
import {Colors, Routes} from './utils';

const Stack = createStackNavigator();

const App = () => {
  const {HOME, ACCOUNT_SETUP, ACCOUNT_DETAILS} = Routes;
  return (
    <NavigationContainer>
      <StatusBar
        backgroundColor={Colors.screenContainerBg}
        barStyle="dark-content"
      />
      <Stack.Navigator initialRouteName={HOME} headerMode="none">
        <Stack.Screen name={HOME} component={Home} />
        <Stack.Screen name={ACCOUNT_SETUP} component={AccountSetup} />
        <Stack.Screen name={ACCOUNT_DETAILS} component={AccountDetails} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
